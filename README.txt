This (uc_affiliate2) is a module providing affiliate functionality for Ubercart.

Initial activity started here:
http://www.ubercart.org/forum/support/1586/how_are_people_doing_affilate_stuff_ubercart
Then moved here:
http://www.ubercart.org/contrib/2446

Code by
********
joe turgeon (http://arithmetric.com)
jhuckabee
VitaLife
thierry_gd (from Drupal affiliate module)
davegan
bojanz (http://vividintent.com)

Features
********
User based tracking
Role based tracking
Monthly report of sales/commissions per affiliate in the admin section
Ability to create affiliate links (both text and image)
Product level link generation (e.g. generate a link for the actual product vs just the website), export to CSV or HTML
Ability to add cookies so that affiliates get credit for return visits even though they didn't go through original affiliate link.
Easy customer dashboard
Per product commissions
Multiple levels of commissioning

Notes
*****
- The subdomain affiliate link handling is disabled by default from version 1.4.
You can enable it from the Affiliate Settings page in Ubercart admin.
- The admin ( user uid: 1) can't be an affiliate
- Don't give all authenticated users the affiliate permission. You need to create a role.
- Create a role, give it affiliate permission, and then assign users to that role.
If you assign two roles with affiliate permissions to a user, bad things happen :)

Upgrading to 1.7
****************
- You must resave your affilite settings for the commission cancellation to work
- Commission cancellation (for previously added orders) might leave incorrect admin counters, because of a earlier time&date bug
- The orders counter works only for orders added after the upgrade
- The "All orders" tab shows only the orders added after the upgrade.

Sponsors
********
Aaron Schiff (http://www.ecopaper.com/)
Jason Graham (http://polishyourimage.com)
A Mountain Top, LLC (http://amountaintop.com)
You?
